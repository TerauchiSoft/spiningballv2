﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroudCollider : MonoBehaviour
{
    public SphereMove sphereMove;
    public void OnTriggerEnter(Collider other) {
        if (other.tag == "Ground")
            sphereMove.isGround = true;
    }
    public void OnTriggerStay(Collider other) {
        if (other.tag == "Ground")
            sphereMove.isGround = true;
    }
    public void OnTriggerExit(Collider other) {
        if (other.tag == "Ground")
            sphereMove.isGround = false;
    }
}
