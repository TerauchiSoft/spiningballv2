﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Checker : MonoBehaviour {
    public GameObject[] objs;
    public Text cleartext;
    private void Update() {
        foreach (var o in objs) {
            if (o != null)
                return;
        }

        cleartext.gameObject.SetActive(true);
        Destroy(gameObject);
    }
}
